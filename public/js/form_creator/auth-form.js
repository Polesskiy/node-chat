/**
 * Authorization form class.
 *
 * Allow to render sign up or sign in form,
 * validate user registration/authorization data from input fields,
 * send user data to server by AJAX.
 */
class AuthForm {
    constructor(formName) {
        this.USERS_URL = "/users";

        //form edge
        this._formRow = $('<div>', {class: "row centered-form", name: "authForm"});
        this._panelGrid = $('<div>', {class: "col-xs-12 col-sm-8 col-md-4 col-sm-offset-2 col-md-offset-4"});
        this._panel = $('<div>', {class: "panel panel-default"});
        this._panelHeading = $('<div>', {class: "panel-heading"}).html('<h3 class="panel-title">' + formName + '</h3>');
        this._panelBody = $('<div>', {class: "panel-body"});
        this._form = $('<form>', {class: "form-singin", role: "form"});

        //form inputs
        this._loginInput = $('<input>', {
            type: "text",
            name: "login",
            // id:"",
            class: "form-control",
            placeholder: "Login"
        });
        this._emailInput = $('<input>', {
            type: "text",
            name: "email",
            // id:"",
            class: "form-control",
            placeholder: "E-mail"
        });
        this._passwordInput = $('<input>', {
            type: "password",
            name: "password",
            // id:"",
            class: "form-control",
            placeholder: "Password"
        });
        this._passwordConfirmInput = $('<input>', {
            type: "password",
            name: "confirmPassword",
            // id:"",
            class: "form-control",
            placeholder: "Confirm your password"
        });
        this._sendUserButton = $('<input>', {
            type: "button",
            // id:"",
            value: formName,
            class: "btn btn-primary btn-block"
        });
        this._rememberMeCheckBox = $('<input>', {
            type: "checkbox",
            value: "remember-me"
        });
    }

    /**
     * POST to server with user data from registration form
     * @param callback need for tests, invokes when server response while exists
     */
    signUp(callback) {
        var user = {
            login: this._loginInput.val(),
            email: this._emailInput.val(),
            password: this._passwordInput.val()
        };

        /** Send user to server*/
        $.post(this.USERS_URL,
            user,
            //server response handler, we need callback  for tests
            function (data) {
                if (callback) {
                    callback(data);
                } else
                    console.log("server response for user POST: " + JSON.stringify(data));
            })
    }

    /** POST to server with user data from authorization form */
    signIn() {
        $.post(this.USERS_URL, {
                login: this._loginInput.val(),
                password: this._passwordInput.val()
            },
            //server response handler
            function (data) {
                if (data == '') {
                }
            })
    }

    /** Render auth form template */
    renderTemplate() {
        this._panelBody.append(this._form);
        this._panel.append(this._panelHeading);
        this._panel.append(this._panelBody);
        this._panelGrid.append(this._panel);
        this._formRow.append(this._panelGrid);
    }

    /**
     * Render Sign up form in container
     * @param containerId
     */
    renderSignIn(containerId) {
        //create form template
        this.renderTemplate();

        //create checkbox div
        var checkBoxDiv = $('<div>', {class: "checkbox"}).append(
            $('<label>').append(this._rememberMeCheckBox, " Remember me"
            )
        );

        //add appropriate inputs to form
        this._form.append(this._loginInput);
        this._form.append(this._passwordInput);
        this._form.append(checkBoxDiv);
        this._form.append(this._sendUserButton);

        //set onClick handler
        this._sendUserButton.click(function () {
            this.signIn()
        }.bind(this));

        //render to container
        $("#" + containerId).append(this._formRow);
    }

    /**
     * Render sign in form in container
     * @param containerId
     */
    renderSignUp(containerId) {
        this.renderTemplate();

        this._form.append(this._loginInput);
        this._form.append(this._emailInput);
        this._form.append(this._passwordInput);
        this._form.append(this._passwordConfirmInput);
        this._form.append(this._sendUserButton);

        //set onClick handler
        this._sendUserButton.click(function () {
            this.signUp()
        }.bind(this));

        $("#" + containerId).append(this._formRow);
    }
}

