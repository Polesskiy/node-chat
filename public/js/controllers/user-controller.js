var userController = (function () {
    var USERS_URL = "/users";

    /**
     * Client-side user validation
     * @param user user obj
     * @param successValidate callback function while user validation success
     * @param errValidate callback function while user validation error
     */
    function userValidation(user, successValidate, errValidate) {

    }

    return {
        /**
         * Save user to DB
         * POST request
         * @param user user obj to save
         * @param successSave callback function while server response OK for user saving
         * @param errSave callback function while server response ERROR for user saving
         */
        saveUser: function (user, successSave, errSave) {

        },

        /**
         * Delete single user from DB
         * DELETE request
         * @param userLogin
         * @param successDelete callback function while server response OK for user deleting
         * @param errDelete callback function while server response NOT OK for user deleting
         */
        deleteUser: function (userLogin, successDelete, errDelete) {
            $.ajax({
                url: USERS_URL + '/' + userLogin,
                type: 'DELETE',
                success: ()=> successDelete ? successDelete() : window.location.reload(true),
                error: (resp) => errDelete ? errDelete(resp) : console.error("Server response for deleting user " + userLogin + ":\r\n" + resp)
            });
        },

        /**
         * Delete all users from DB
         * DELETE request to server for deleting all users
         * Refresh page after request complete
         * @param successDelete callback function while server response OK for ALL users deleting
         * @param errDelete callback function while server response NOT OK for ALL users deleting
         */
        deleteAllUsers: function (successDelete, errDelete) {
            $.ajax({
                url: USERS_URL,
                type: 'DELETE',
                success: ()=>successDelete ? successDelete() : window.location.reload(true),
                error: (resp) => errDelete ? errDelete(resp) : console.error("Server response for deleting all users request:\r\n" + resp)
            })
        },

        /**
         * Update single user in DB
         * PUT request
         * @param user updated user
         * @param successUpdate callback function if server response OK for user updating
         * @param errUpdate callback function if server response NOT OK for user updating
         */
        updateUserInDB: function (user, successUpdate, errUpdate) {
            $.ajax({
                url: USERS_URL + '/' + user.login,
                type: 'PUT',
                data: user,
                success: ()=>successUpdate ? successUpdate() : window.location.reload(true),
                error: (resp) => errUpdate ? errUpdate(resp) : console.error("Server response for edit user\r\n" + resp)
            })
        }
    }
})();