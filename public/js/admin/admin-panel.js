/**
 * Module with useful functions for admin panel
 */
var adminPanel = (function () {
    const USERS_URL = "/users";
    const NON_EDITABLE_PROPERTIES = ['_id', 'login', '__v'];

    /**
     * Create table header from user obj
     * @param user user obj
     * @returns {*|jQuery|HTMLElement} table header
     */
    function createUserTableHeader(user) {
        //table head cells from user keys
        var headCells = Object.keys(user).map(
            (key) => $('<th>').text(key)
        );

        headCells.push($('<th>').text('Save'));
        headCells.push($('<th>').text('Delete'));

        return $('<thead>').append($('<tr>').append(headCells));
    }

    /**
     * Create table row with user's data from user obj
     * @param user user obj
     * @returns {*|jQuery|HTMLElement} table row
     */
    function createUserTableRow(user) {
        //row cells arr
        var cells = Object.keys(user).map(
            (key)=> {
                var cell = $('<td>').text(user[key]);

                /** bind listener on cell with allowed to edit user obj property
                 * make cell editable by mouse click,
                 * save value and disable editable option
                 * while cell loose focus or enter button pressed
                 */
                if (!NON_EDITABLE_PROPERTIES.some((property)=>key == property)) {
                    cell.on('click',
                        ()=> {
                            var newValueInput = $('<input>', {type: 'text'})
                                .blur(()=> {
                                    user[key] = newValueInput.val();
                                    cell.text(newValueInput.val());
                                })
                                .keypress((e)=> {
                                    if (e.which == 13) {
                                        user[key] = newValueInput.val();
                                        cell.text(newValueInput.val());
                                    }
                                });
                            cell
                                .empty()
                                .append(newValueInput)
                                .unbind();
                            newValueInput.focus();
                        }
                    );
                }
                return cell;
            }
        );

        //service buttons
        var saveUpdatedUserButton = $('<span>', {class: "glyphicon glyphicon-upload"}).on('click', ()=>userController.updateUserInDB(user));
        var removeUserButton = $('<span>', {class: "glyphicon glyphicon-remove"}).on('click', ()=>userController.deleteUser(user.login));

        return $('<tr>').append(
            cells,
            $('<td>').append(saveUpdatedUserButton),
            $('<td>').append(removeUserButton)
        );
    }

    /**
     * Create table from users
     * @param users users array
     */
    function createUsersTable(users, containerId) {
        if (users && users.length) {
            var usersTable = $('<table>', {class: "table table-stripped text-centered"});

            usersTable.append(
                //head
                createUserTableHeader(users[0]),
                //body
                $('<tbody>').append(
                    /** table rows from users properties*/
                    users.map((user)=>createUserTableRow(user))
                )
            );

            $('#' + containerId).append(usersTable);
        }
    }

    return {
        /**
         * Render users table - get and render them to container as table
         * @param containerId container ID to render in
         */
        renderUsersTable: function (containerId) {
            //clear container
            $('#' + containerId).empty();

            $.get(USERS_URL, null, (users) => createUsersTable(users, containerId));
        },

        /**
         * Render table with single user
         * @param usersLogin users login to find by
         * @param containerId container ID to render in
         */
        renderSingleUser: function (usersLogin, containerId) {
            $('#' + containerId).empty();
            $.get(USERS_URL + '/' + usersLogin, null, (user) => createUsersTable([user], containerId));
        },

        /**
         * Create some test users in DB
         * POST request to server with test users obj to create them in
         */
        createTestUsers: function () {
            var testUsers = [
                {login: "lorem", email: 'ipsum', password: 'dolor'},
                {login: "sit", email: 'amet', password: 'consectetur'}
            ];

            //request counter
            var usersRequestCounter = testUsers.length;

            testUsers.forEach((user)=>
                $.ajax({
                    type: 'POST',
                    url: USERS_URL,
                    data: user,
                    //when all requests will be done - reload page
                    success: () => (--usersRequestCounter) ? $.noop() : window.location.reload(true),
                    error: (resp) => (--usersRequestCounter) ? console.error(resp) : window.location.reload(true)
                })
            )
        }
    }
}());