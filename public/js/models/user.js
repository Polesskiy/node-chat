/**
 * User class
 */
class User {
    /** regexp for validation*/
    //TODO const static obj class field
    var regexp = {
        EMAIL: "\S+@\S+\.\S+/",
        LOGIN: "/\S+/",
        PASSWORD: "/\S+/"
    }

    /**
     * User constructor
     * @param login
     * @param email
     * @param password
     */
    constructor(login, email, password) {
        this.login = login;
        this.email = email;
        this.password = password;
    }

    /**
     * Create user from user schema (from DB)
     * @param userSchema
     * @returns {User} new user onj
     */
    static createFromUserSchema(userSchema) {
        return new User(userSchema.login, userSchema.email, userSchema.password);
    }

    /**
     * Self validation
     * @returns validation represent obj:
     */
    validate() {
        return {
            login: (this.login && User.regexp.LOGIN.test(this.login)),
            email: (this.email && User.regexp.EMAIL.test(this.email)),
            password: (this.password && User.regexp.PASSWORD.test(this.password))
        };
    }

    toString() {
        return JSON.stringify(this);
    }
}
