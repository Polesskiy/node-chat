var LocalStrategy = require('passport-local').Strategy;
var User = require('../models/user-schema');

module.exports = function (passport) {

    passport.use('signup', new LocalStrategy({
            passReqToCallback: true // allows us to pass back the entire request to the callback
        },
        function (req, login, password, done) {

            findOrCreateUser = function () {
                // find a user in Mongo with provided login
                User.findOne({'login': login}, function (err, user) {
                    // In case of any error, return using the done method
                    if (err) {
                        console.log('Error in SignUp: ' + err);
                        return done(err);
                    }
                    // already exists
                    if (user) {
                        console.log('User already exists with login: ' + login);
                        return done(null, false, null /*req.flash('message','User Already Exists')*/);
                    } else {
                        // create the user
                        var newUser = new User();

                        // set the user's local credentials
                        newUser.login = login;
                        newUser.password = createHash(password);
                        newUser.email = req.param('email');

                        // save the user
                        newUser.save(function (err) {
                            if (err) {
                                console.log('Error in Saving user: ' + err);
                                throw err;
                            }
                            console.log('User Registration successful');
                            return done(null, newUser);
                        });
                    }
                });
            };
            // Delay the execution of findOrCreateUser and execute the method
            // in the next tick of the event loop
            process.nextTick(findOrCreateUser);
        })
    );

    /** Generate hash*/
    //TODO remove mock
    var createHash = function (password) {
        //return bCrypt.hashSync(password, bCrypt.genSaltSync(10), null);
        return password; //mock
    }
};